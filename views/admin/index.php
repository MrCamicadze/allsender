<?php  include_once 'views/layout/header.php'; ?>


                 
<div class="location">
    <h1>Админ панель</h1>
</div>
<div class="content">
    <div class="mainUser t1">
        
        
        <table class="bordered">
            <thead>
                <tr>
                    <th style="width:40px">№</th>
                    <th style="width:120px">Дата</th>
                    <th style="width:80px">Пользователь</th>
                    <th style="width:40px">Сумма</th>
                    <th style="width:104px">Статус</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($paysInfo as $payInfo):
                    ?>

                    <tr>
                        <td><?php echo $payInfo['id']; ?></td>
                        <td><?php echo $payInfo['datetime']; ?></td>
                        <td><?php echo $payInfo['username']; ?></td>
                        <td><?php echo $payInfo['value']; ?> $</td>
                        <td><?php switch ($payInfo['stat']){
                            case 'success': echo 'Успешный платеж'; break;
                            case 'failure': echo 'Неуспешный платеж'; break;
                            case 'error': echo 'Некорректно заполнены данные'; break;
                            case 'reversed': echo 'Платеж возвращен'; break;
                            case 'sandbox': echo 'Тестовый платеж'; break;
                        }?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <table class="bordered ">
            <thead>
                <tr>
                    <th style="width:100px">Пользователь</th>
                    <th style="width:100px">Доступ</th>
                    <th style="width:100px">Баланс</th>
                    <th style="width:100px">Управление</th>
                    <th style="width:100px"></th>

                </tr>
            </thead>
            <tbody>
                <?php foreach ($usersinfo as $userinfo):
                    ?>

                    <tr>
                        <td><?php echo $userinfo['username']; ?></td>
                        <td><?php switch ($userinfo['type']){
case 1: echo 'юзер';
break;
case 2: echo 'модератор';
break;
case 3: echo 'администратор';
break;

} ?></td>
                        <td><?php echo $userinfo['balance']; ?> $</td>
                        <td>
                            <a  class="edit_user"href="#" id="<?php echo $userinfo['id'];?>&edit=ok">Изменить</a>
            
                            
                        </td>
                        <td>
                        <a class="delete_user" href="#" id="<?php echo $userinfo['id'];?>&delete=ok">Удалить</a>
                    </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div><div class="mainUser t1">
        <table class="bordered t1">
            <thead>
                <tr>
                    <th style="width:40px">№</th>
                    <th style="width:120px">Информация</th>
                    <th style="width:150px">Состояние</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($clientsList as $client):
                    if ($client['site'] == 'victoriyaclub') {
                        $color = '#0161bd';
                    } else if ($client['site'] == 'loveinchat') {
                        $color = '#ff4b16';
                    } else {
                        $color = '#8e66c3';
                    }
                    ?>
                    <tr style="color:<?php echo $color; ?>;">
                        <td style="text-align:center;"><?php echo $client['id']; ?></td>
                        <td style="text-align:center;">Пользоваель: <?php echo $client['username']; ?></br>Сайт: <?php echo $client['site']; ?></br>ID: <?php echo $client['id_name']; ?></td>
                      
                        <td style="text-align:center;">
                            <?php
                            switch ($client['work']) {
                                case 3:
                                    echo '<p>Ожидает очереди</p><p>Начало - '.$client['date'].'</p>';
                                    break;
                                case 5:
                                    echo '<p>В работе</p><p>Начало - '.$client['date'].'</p>
                                             ' . $client["work_from"] . "/" . $client["work_to"] . '
                                            <progress value="' . $client['work_from'] . '" max="' . $client["work_to"] . '"></progress>';
                                    break;
                            }
                            ?>
                            
                            <p>Стоимость рассылки: <?php echo $client['cost'];?> $</p>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        
  <table class="bordered t1">
            <thead>
                <tr>
                    <th style="width:70px">Дата</th>
                    <th style="width:50px">Сумма</th>
                </tr>
            </thead>
            <tbody>
                <?php $sumcost = 0; 
foreach ($payDays as $payDay):
                    ?>

                    <tr>
                        <td><?php echo $payDay['date']; ?></td>
                        <td><?php echo $payDay['cost']; $sumcost +=$payDay['cost']; ?> $</td>

                    </tr>
                <?php endforeach; ?>
<tr>
                        <td>Итого:</td>
                        <td><?php echo $sumcost; ?> $</td>

                    </tr>
            </tbody>
        </table>  
          
        
        
        
        
        
        
        
             
        
        
              <div class="overlay" title="окно"></div>
            <div class="popup">
                <div class="close_window">x</div>
                <div id="media-library"></div>
            </div>
            <div id="result"></div>
        
            
            
     
            
    </div>
</div>
         
<?php include_once 'views/layout/footer.php';?>