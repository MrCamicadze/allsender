<?php
include_once 'views/layout/header.php';

if (isset($errors) && is_array($errors)):
    ?>
    <p class="error"> 
        <?php foreach ($errors as $error): ?>
            <?php echo $error; ?><br>
        <?php endforeach; ?>
    </p>
<?php endif; ?>
<div class="content">   
    <div class="container mlogin">
        <div id="login">
            <h1>Настройки аккаунта</h1>
            <input type="email" name="email" class="input" value="<?php echo $accountInfo["email"]; ?>"  disabled/>
            <input type="text" name="username" class="input" value="<?php echo $accountInfo["username"]; ?>" disabled />
            <form name="saveaccount" id="accountform" action="" method="post">
                <input type="password" name="oldpassword" placeholder="Старый пароль" class="input" value=""  />
                <input type="password" name="password" placeholder="Новый пароль" class="input" value=""  />
                <input type="password" name="password2" placeholder="Подтверждение нового пароля" class="input"  value="" />
                <p class="submit">
                    <button type="submit" name="save" class="button" value="Сохранить">Сохранить</button>
                </p>
            </form>
        </div>
    </div>
</div>




<?php include_once 'views/layout/footer.php'; ?>
    