<?php include_once 'views/layout/header.php';
if (isset($errors) && is_array($errors)):?>
    <p class="error"> 
        <?php foreach ($errors as $error):?>
            <?php echo $error; ?><br>
        <?php endforeach;?>
    </p>
<?php endif;?>
    <div class="content">
<div class="container mlogin">
    <div id="login">
        <h1>Авторизация</h1>
        <form name="loginform" id="loginform" action="" method="POST">
            <input type="text" placeholder="Логин" name="username" class="input" value="" />
            <input type="password" placeholder="Пароль" name="password" class="input" value="" />
            <p class="submit">
                <button type="submit" name="login" class="button" value="go">Войти</button>
            </p>
            <p class="regtext">Еще нет учетной записи? <a href="../register/" >Регистрация!</a></p>
        </form>
    </div>
</div>
    </div>
	
<?php include_once 'views/layout/footer.php'; ?>