<?php
include_once 'views/layout/header.php';

if (isset($errors) && is_array($errors)):
    ?>
    <p class="error"> 
        <?php foreach ($errors as $error): ?>
            <?php echo $error; ?><br>
        <?php endforeach; ?>
    </p>
<?php endif; ?>
<div class="content">   
    <div class="container mlogin">
        <div id="login">
            <h1>Авторизация на сайте</h1>
            <form name="loginform" id="loginform" action="" method="POST">
                <select class="select" name="site">
                    <option selected value="victoriyaclub">VictoriyaClub</option>
                    <option value="loveinchat">LoveInChat</option>
                    <option value="tenderbride">TenderBride</option>
                </select>
                <input type="text" placeholder="Логин" name="username" class="input" value="" size="20" />
                <input type="password" placeholder="Пароль" name="password" class="input" value="" size="20" />
                <p class="submit">
                    <button type="submit" name="mylogin" class="button" value="go">Войти</button>
                </p>
            </form>
        </div>
    </div>
</div>
    
<?php include_once 'views/layout/footer.php'; ?>

