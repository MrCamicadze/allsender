<?php include_once 'views/layout/header.php';?>
<?php echo($response); if ( ($editClient['work']!=6)) $dis='disabled'; $disf='disabled'; ?>
<link rel="stylesheet" href="/template/css/media-library.css">

<div class="location">
    <h1>Редактирование рассылки.</h1>
</div>
<div class="content">
    <div class="mainEdit">
        <form name="editform" id="editform" action="" method="POST">
            <p>
                <input <?php echo $dis;?> type="text" name="title" id="title" placeholder="Тема сообщения" class="input" value="<?php echo $editClient['title'];?>" />
            </p>
            <p>
                <textarea <?php echo $dis;?> name="text" class="textarea" placeholder="Сообщение" eform="Текст сообщения:string:1" ><?php echo $editClient['text'];?></textarea>
            </p>




            
            <input type="hidden" name="id" value="<?php echo $editClient['id'];?>" />
            <input type="hidden" name="ref" value="<?php echo $_SERVER['HTTP_REFERER'];?>" />
            <input type="hidden" name="v" id="c" value="pause_edit" />

            <?php include_once 'views/user/photos_tpl.php';?>

            </div>
            <p>
                <textarea <?php echo $disf;?> name="idlist" class="textarea" placeholder="ID для рассылки" eform="Текст сообщения:string:1"><?php echo $editClient['id_send'];?></textarea>
            </p>
            <label>Выберите дату и время начала рассылки:</label>
            <input type="date" id="date" name="date" value="<?php echo date('Y-m-d');?>" min="<?php echo date('Y-m-d');?>"/>
           
            
            
            <select name="hour">
                <?php 
                 $hour = date('H');
                 for ($i = 0; $i < 24; $i++) {
                    if ($i < 10){
                        $i = '0'.$i;
                    }
                    if ($i!=$hour){
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }else{
                        echo '<option value="'.$i.'" selected>'.$i.'</option>';    
                    }
                 }
                 ?>
            </select> :
            
            <select name="min">
                <?php 
                 $min = date('i');
                 for ($i = 0; $i <= 55; $i+=5) {
                    if ($i < 10){
                        $i = '0'.$i;
                    }
                    if ((($min - $i)==0)or((($i - $min)<5)and(($i - $min)>0)) ){
                        echo '<option value="'.$i.'" selected>'.$i.'</option>';
                    }else{
                        echo '<option value="'.$i.'">'.$i.'</option>';
                            
                    }
                 }
                 ?>  
            </select>
            
            
            
            
            <p class="submit">
                <button <?php echo $dis;?> type="submit" name="myedit" class="button" value="Save">Сохранить</button>
            </p>
        </form>
    </div>
</div>

    <div class="editcontainer">
        
    </div>
<?php include_once 'views/layout/footer.php';?>
