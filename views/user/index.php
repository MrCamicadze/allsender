<?php include_once 'views/layout/header.php';   ?>
<div class="location">
    <h1>Панель управления</h1>
</div>
<div class="content">
    <div class="rightCol">
        <ul class="rightNav">
            <li><a href="/user/1/" <?php if ($actionId == 1){echo'class="active"';} ?>>Все задания</a></li>
            <li><a href="/user/2/" <?php if ($actionId == 2){echo'class="active"';} ?>>Новые задания</a></li>
            <li><a href="/user/3/" <?php if ($actionId == 3){echo'class="active"';} ?>>На модерации</a></li>
            <li><a href="/user/4/" <?php if ($actionId == 4){echo'class="active"';} ?>>В очереди</a></li>
            <li><a href="/user/5/" <?php if ($actionId == 5){echo'class="active"';} ?>>В работе</a></li>
            <li><a href="/user/6/" <?php if ($actionId == 6){echo'class="active"';} ?>>На паузе</a></li>
            <li><a href="/user/7/" <?php if ($actionId == 7){echo'class="active"';} ?>>Выполненная рассылка</a></li>
        </ul>
        <?php include_once 'views/layout/rightBlock.php';?>
    </div>
    <div class="mainUser">
        <table class="bordered">
            <thead>
                <tr>
                    <th style="width:40px">№</th>
                    <th style="width:120px">Пользоваель</th>
                    <th style="width:400px">Сообщение</th>
                    <th style="width:150px">Состояние</th>
                </tr>
            </thead>
            <tfoot align="center">
                <tr>
                    <td colspan="4"><?php echo $pagination->get(); ?>   </td>
                </tr>
            </tfoot>
            <tbody>
                <?php
                foreach ($clientsList as $client):
                    if ($client['site'] == 'victoriyaclub') {
                        $color = '#0161bd';
                    } else if ($client['site'] == 'loveinchat') {
                        $color = '#ff4b16';
                    } else {
                        $color = '#8e66c3';
                    }
                    ?>
                    <tr style="color:<?php echo $color; ?>;">
                        <td style="text-align:center;"><?php echo $client['id']; ?></td>
                        <td style="text-align:center;">Сайт: <?php echo $client['site']; ?></br>ID: <?php echo $client['id_name']; ?></br>Имя: <?php echo $client['name']; ?></br><img src="<?php echo $client['photo']; ?>" height="150px"/></td>
                        <td>Заголовок:  <?php echo $client['title']; ?><p>Текст письма:  <?php echo $client['text']; ?></p>

<?php 
if (strlen($client['message_photo_id'])>0) $client['message_photo_1'] = $client['message_photo'];
if (strstr($client['message_photo_id'],'video') !== false) {
    
    for ($i=1; $i < 6; $i++) { 
        if(strlen($client['message_photo_'.$i])>0) {
            echo "<div class=\"user-photo\" data-type=\"video\" data-approved=\"Y\">
            <label class=\"user-photo-label\">
                <div class=\"user-photo-img-div\">
                    <div class=\"user-photo-img-div-inner\">
                        <img src=\"{$client['message_photo_id_'.$i]}\" width=\"100px\" align=\"middle\"/>
                    </div>
                </div>
            </label>
        </div>";
        }
    }

} else {
    for ($i=1; $i < 6; $i++) { 
        if(strlen($client['message_photo_'.$i])>0) {
            echo "<img src=\"{$client['message_photo_'.$i]}\" width=\"100px\" align=\"middle\"/>";
        }
    }
}

?>

</td>
                        <td style="text-align:center;">
                            <?php
                            switch ($client['work']) {
                                case 0:
                                    echo '<form action="/user/edit/" method="post">
                                    <input type="hidden" name="id" value="' . $client['id'] . '">
                                    <p class="submit"><button type="submit"  name="v" value="go">Отправить на проверку</button></p>

                                    <p class="submit"><button type="submit"  name="v" value="edit">Редактировать</button></p>
                                    </form>';
                                    break;
                                case 1:
                                    echo 'На модерации';
                                    break;
                                case 3:
                                    echo '<p>Ожидает очереди</p><p>Начало - '.$client['date'].'</p>';
                                    break;
                                case 5:
                                    echo '<p>В работе</p>
                                             ' . $client["work_from"] . "/" . $client["work_to"] . '
                                            <progress value="' . $client['work_from'] . '" max="' . $client["work_to"] . '"></progress>'
                                        . '<form action="/user/edit/" method="post">
                                    <input type="hidden" name="id" value="' . $client['id'] . '">
                                    <p class="submit"><button type="submit"  name="v" value="pause">Поставить на паузу</button></p>
                                    </form>';
                                    break;
                                case 6:
                                    echo '<p>На паузе</p>
                                             ' . $client["work_from"] . "/" . $client["work_to"] . '
                                            <progress value="' . $client['work_from'] . '" max="' . $client["work_to"] . '"></progress>'
                                        . '<form action="/user/edit/" method="post">
                                    <input type="hidden" name="id" value="' . $client['id'] . '">
                                        <p class="submit"><button type="submit"  name="v" value="pause_edit">Редактировать</button></p>
                                        
                                    <p class="submit"><button type="submit"  name="v" value="start">Убрать с паузы</button></p>
                                    </form>';
                                    break;
                                case 9:
									if ($accountInfo['type']==3){
										echo '<form action="/user/edit/" method="post">
                                    <input type="hidden" name="id" value="' . $client['id'] . '">
                                    <p class="submit"><button type="submit"  name="v" value="edit">Редактировать</button></p>
                                    </form>';
									}
                                    echo 'Выполненная рассылка';
                                    break;
                            }
                            ?>
                            <p>Стоимость рассылки: <?php echo $client['cost'];?> $</p>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>                 
    </div>
</div>
         
<?php include_once 'views/layout/footer.php';?>
