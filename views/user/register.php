<?php include_once 'views/layout/header.php';
if (isset($errors) && is_array($errors)):?>
    <p class="error"> 
        <?php foreach ($errors as $error):?>
            <?php echo $error; ?><br>
        <?php endforeach;?>
    </p>
<?php endif;?>
    <div class="content">
<div class="container mlogin">
    <div id="login">
	<h1>Регистрация</h1>
        <form name="registerform" id="registerform" action="" method="post">
            <input type="text" name="username" placeholder="Логин" class="input" value="<?php echo $username;?>"  />
            <input type="email" name="email" placeholder="Почта" class="input" value="<?php echo $email;?>"  />
            <input type="password" name="password" placeholder="Пароль" class="input" value="" />
            <p class="submit">
                <button type="submit" name="register" class="button" value="go">Регистрация</button>
            </p>
            <p class="regtext">Уже есть учетная запись? <a href="../login/" >Войти!</a></p>
        </form>
    </div>
</div>
    </div>
	
<?php include_once 'views/layout/footer.php'; ?>