<?php include_once 'views/layout/header.php'; ?>
<div class="content">
    
    <?php
    session_start();
    if (!User::accountInfo()['type']>=1):?>
    <div class="rightCol">
        <div class="block">
            <h3>Авторизация</h3>
            <div class="containerLogin">
                <form name="loginform"  action="/user/login/" method="POST">
                    <input type="text" placeholder="Логин" name="username" class="input" value="" />
                    <input type="password" placeholder="Пароль" name="password" class="input" value="" />
                    <button type="submit" name="login" class="button" value="go">Войти</button>
                    Еще нет учетной записи? <a href="/user/register/" >Регистрация!</a>
                </form>
            </div>
        </div>
    </div>
    <?php endif;?>
    <div class="main">
       <!-- <h1>What we do</h1-->
        <p>Добро пожаловать в наш сервис рассылки сообщений по брачным агентствам. Вы оплачиваете только те письма что действительно были отправлены.
У нас действует гибкая система скидок! (чем больше рассылка тем дешевле).
        </p>
        <p><strong>Таблица стоимости оправленых сообщений:</strong></p>
<table class="bordered mybordered">
    <thead>
        <tr>
            <th>Количество ID в одной рассылке</th>
            <th>Стоимость за 1 сообщение</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>до 500 шт</td>
            <td>0.0003$</td>
        </tr>
        <tr>
            <td>до 750 шт</td>
            <td>0.00025$</td>
        </tr>
        <tr>
            <td>от 751 шт</td>
            <td>0.0002$</td>
        </tr>
    </tbody>
</table>

        <!--Изображения-->
        <p><strong>Работаем с:</strong></p>
        <p class="psite"><img src="/template/images/victoriyaclub.png"><img src="/template/images/loveinchat.png"><img src="/template/images/tenderbride.png"></p>

        <!--Формы-->
        <!--<div class="psite">
            <a href="https://rdr.salesdoubler.com.ua/in/offer/1110?aid=67452"><img src="https://s3.amazonaws.com/salesdoubler/banner_creatives/banners/65254/original.jpg"/></a>

        </div>
        <div class="container pay">
            <h1>Остались вопросы?</h1>
            <form action="/" method="POST">
                <input type="text" class="input" placeholder="Teма сообщения" name="subject" value="">
                <input type="Е-mail" class="input" placeholder="Ваш email" name="email" value="">
                <textarea name="message" class="textarea" placeholder="Текст вашего сообщения"></textarea>
                <button type="submit" class="button" name="callback" value="true">Задайте их нам</button>
            </form>
        </div>-->
         
        
    </div>
</div>
<?php include_once 'views/layout/footer.php'; ?>
