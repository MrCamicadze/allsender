<div class="block">
    <p class="more">31.03.2021 Закрытие!</p>
</div>
<div class="block">
    <h3>Добро пожаловать: <?php echo $accountInfo["username"]; ?></h3>
    <p>Баланс: <?php echo $accountInfo["balance"]; ?> $ </p>
    <p>Сегодня: <?php echo date('d-m-Y');?></p>
<form action="/pay/" method="POST">
<p><button type="submit" >Пополнить аккаунт</button></p><p></p>
    </form>
</div>

<div class="block">                      
    <form name="statform" id="statform" action="/stat/" method="POST">
        <p>
        <select name="month">
            <?php 
            $MonthArray = array("1"=>"Январь","2"=>"Февраль","3"=>"Март",
                    "4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
                    "7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
                    "10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь"
                    );
            foreach ($MonthArray as $keymonth =>$month) {
                if (isset($_POST['month'])){
                    $selected = ($keymonth == $_POST['month']) ? 'selected' : '';
                }else{
                    $selected = ($keymonth == date('m')) ? 'selected' : '';
                }
                $keymonth = str_pad($keymonth, 2, "0", STR_PAD_LEFT);
                echo '<option '.$selected.' value="'.$keymonth.'">'.$month.'</option>';
            }?>
        </select>
        <select name="year">
            <?php
                    $yearArray = range(2018, 2025);

            foreach ($yearArray as $year) {
                if (isset($_POST['month'])){
                    $selected = ($year == $_POST['year']) ? 'selected' : '';
                }else{
                    $selected = ($year == date('Y')) ? 'selected' : '';
                }
                echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
            }?>
        </select>
        </p>
        <p><button type="submit"  name="stat" value="stat">Показать статистику</button></p>
    </form>
    <p class="more">Расход за месяц: <?php echo Stat::getStatCost($accountInfo['id']);?> $</p>
</div>

<div class="block">
    <p>Контакты для связи с администрацией:</p>
    <p>тел.: +380991234567</p>
    <p class="more">E-mail: example@gmail.com</p>
</div>