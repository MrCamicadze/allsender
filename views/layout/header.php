<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="/template/css/site_style.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <title>SMC project by MaLuTkA</title>
    </head>
    
    <body>
        <div class="wrapper">
		<div class="wrapper">

            <header>
                <div class="logo"><a href="/">S<span class="black">M</span>C<span class="gray">.pp.ua</span></a></div>
                <ul class="nav">
                    <?php 
                    if ($accountInfo['type']==3){
                        echo'<li><a href="/admin/">Админка</a></li>';
                    }
                    if ($accountInfo['type']>=2){
                        echo'<li><a href="/moder/">Модерация</a></li>';
                    }
                    if ($accountInfo['type']>=1){
                        echo'<li><a href="/user/">Главная</a></li>
                            <li><a href="/user/create/">Создать аккаунт для рассылки</a></li>
                            <li><a href="/user/account/">Настройки</a></li>
                            <li><a href="/user/logout/">Выход</a></li>';
                    }else{
                        echo'<li><a href="/user/">Панель управления</a></li>';
                    }
                    ?>
                </ul>
            </header>
            