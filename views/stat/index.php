<?php include_once 'views/layout/header.php'; ?>

<div class="location">
    <h1>Статистика отправленных сообщений</h1>
</div>
<div class="content">
    <div class="rightCol">
        
        <?php include_once 'views/layout/rightBlock.php';?>
    </div>
    <div class="mainUser mainStat">
        <table class="bordered">
            <thead>
                    <tr>
                        <th style="width:150px">Пользоваель</th>
                        <th style="width:150px">Дата</th>
                        <th style="width:50px">№</th>
                        <th style="width:110px">Отправленно</th>
                        <th style="width:110px">Стоимость</th>
                        <th style="width:110px">Всего</th>
                    </tr>
                </thead>
                <tbody>
<?php
    foreach ($statList as $client):
        if ($client['site'] == 'victoriyaclub') {
            $color = '#0161bd';
        } else if ($client['site'] == 'loveinchat') {
            $color = '#ff4b16';
        } else {
            $color = '#8e66c3';
        }
        $infopost=array();
        $clientCost=0;
        foreach ($client['stat'] as $clientStat):
            $infopost[]='<td>'.$clientStat['date'].'</td>    
                        <td>'.$clientStat['id'].'</td> 
                        <td>'.$clientStat['work_from'].'</td>    
                        <td>'.$clientStat['cost'].' $</td>';
            $allCost=$allCost+$clientStat['cost'];
            $clientCost=$clientCost+$clientStat['cost'];
                                      
        endforeach;
        
        $count = count($client['stat']);
        
        echo '<tr style="color:'.$color.';">'
                . '<td rowspan="'.$count.'" >Сайт: '. $client['site'].'</br>ID: '.$client['id'].'</br>Имя: '.$client['name'].'</br><img src="'.$client['photo'].'" height="150px"/></td>'
                . $infopost['0']
                . '<td rowspan="'.$count.'">'.$clientCost.' $</td>'
                . '</tr>';                 
        
        for ($i=1; $i<=$count; $i++){
            echo '<tr style="color:'.$color.';">'.$infopost[$i].'</tr>';    
            
            
        }
        
        
    endforeach; ?>
                </tbody>
</table>                
    </div>
</div>



<?php include_once 'views/layout/footer.php'; ?>
