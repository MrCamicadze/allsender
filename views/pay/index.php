<?php include_once 'views/layout/header.php'; ?>

<div class="location">
    <h1>Статистика отправленных сообщений</h1>
</div>
<div class="content">
    <div class="rightCol">       
        <?php include_once 'views/layout/rightBlock.php';?>
    </div>
    <div class="container pay">
        <div id="login">
            <h1>Введите сумму пополнения</h1>          
            <form method="POST" action="" accept-charset="utf-8">
                <input name="amount" placeholder="0.00" type="number" class="input" min="1" step = "0.01" onkeyup="this.value=this.value.replace(/[^0-9.]/ig,\'\')" maxlength="4" value="""> 
                <p class="submit">
                    <button type="submit" name="login" class="button" value="go">Пополнить</button>
                </p>
            </form>
            
        </div>
    </div>    
    <div class="mainUser mainStat">
        <table class="bordered">
            <thead>
                    <tr>
                        <th style="width:50px">№</th>
                        <th style="width:150px">Дата</th> 
                        <th style="width:100px">Сумма</th>
                        <th style="width:100px">Статус</th>
                    </tr>
                </thead>
                <tbody>
<?php
    foreach ($payInfo as $check):
        if ($check['stat'] == 'sandbox'){
            $check['stat'] = 'Тестовый платеж';
        }
        if ($check['stat'] == 'success'){
            $check['stat'] = 'Успешный платеж';
        }
        echo '<tr>    
            <td>'.$check['id'].'</td>    
            <td>'.$check['datetime'].'</td> 
            <td>'.$check['value'].' $</td>    
            <td>'.$check['stat'].'</td>
        </tr>';                       
        
    endforeach; ?>
                </tbody>
</table>                
    </div>
    
    
    
</div>

<?php include_once 'views/layout/footer.php'; ?>
