<?php include_once 'views/layout/header.php'; ?>

     
<div class="location">
    <h1>Панель модерации</h1>
</div>
<div class="content">
    <div class="main">
        <table class="bordered">
            <thead>
                <tr>
                    <th style="width:30px">№</th>
                    <th style="width:150px">Пользоваель</th>
                    <th style="width:600px">Сообщение</th>
                    <th style="width:150px">Работа</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($clientsList as $client):
                    if ($client['site'] == 'victoriyaclub') {
                        $color = '#0161bd';
                    } else if ($client['site'] == 'loveinchat') {
                        $color = '#ff4b16';
                    } else {
                        $color = '#8e66c3';
                    }
                    ?>

                    <tr style="display: flex; color:<?php echo $color; ?>;">
                    <td style="text-align:center;"><?php echo $client['id']; ?></td>
                    <td style="text-align:center;">Сайт: <?php echo $client['site']; ?></br>ID:
                        <?php echo $client['id_name']; ?></br>Имя: <?php echo $client['name']; ?></br><img
                            src="<?php echo $client['photo']; ?>" height="150px" /></td>
                    <td style="width: 628px;">Заголовок: <?php echo $client['title']; ?><p>Текст письма:
                            <?php echo $client['text']; ?></p>
                            <?php
                            if (strlen($client['message_photo'])>0) $client['message_photo_1'] = $client['message_photo'];

                            for ($i=1; $i < 6; $i++) { 
                                if(strlen($client['message_photo_'.$i])>0) {
                                    echo "<img src=\"{$client['message_photo_'.$i]}\" width=\"100px\" align=\"middle\"/>";
                                }
                            }
                            ?>
                        
                            
                    
                    </td>
                    <td style="text-align:center; width: 150px;">
                        <form action="/user/edit/" method="post">
                            <input type="hidden" name="id" value="<?php echo $client['id'];?>">
                            <p class="submit"><button type="submit" class="button1" name="v"
                                    value="edit">Редактировать</button></p>
                        </form>
                        <form action="/moder/edit/" method="post">
                            <input type="hidden" name="id" value="<?php echo $client['id'];?>">
                            <p class="submit"><button type="submit" class="button1" name="v" value="ok">Начать
                                    рассылку</button></p>
                            <p>Стоимость рассылки: <?php echo $client['cost'];?> $</p>
                            <p class="submit"><button type="submit" class="button1" name="v"
                                    value="delete">Удалить</button></p>
                        </form>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>                 
    </div>
</div>
         
<?php include_once 'views/layout/footer.php';?>