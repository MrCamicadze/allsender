<?php


class User {
    
    
    const SHOW_BY_DEFAULT = 5;


    public static function checkActionId($actionId){
        switch ($actionId){
        case 2: return 0;
        case 3: return 1;
        case 4: return 3;
        case 5: return 5;
        case 6: return 6;
        case 7: return 9;
        default: return false;
        }
    }
    public static function getOneClient($id){
        $db = Db::getConnection();
        $sql = "SELECT * FROM vc_users WHERE id='".$id."'";
        $result = $db->query($sql);
        $oneClient = $result->fetch();
        return $oneClient;
    }
    
    public static function infoClientsAll($id,$work, $page = 1){
        if ($work !== false) {$work = " AND work='".$work."'";} 
        
        $db = Db::getConnection();
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        if ($id <> 1) {
        $sql = "SELECT id,site, photo,id_name, name, work, work_from, "
                . "work_to, title, text, message_photo,
                message_photo_id, message_photo_id_2, message_photo_id_3, message_photo_id_4, message_photo_id_5, message_photo_2, message_photo_3, message_photo_4, message_photo_5, cost,DATE_FORMAT(`date`, '%d.%m.%Y %H:%i:%s') AS `date` FROM vc_users "
                . "WHERE id_users='".$id."'".$work
                . " ORDER BY id DESC "
                . " LIMIT ".self::SHOW_BY_DEFAULT
                . " OFFSET ".$offset;
		} else {
		$sql = "SELECT id,site, photo,id_name, name, work, work_from, "
                . "work_to, title, text, message_photo,message_photo_id, message_photo_id_2, message_photo_id_3, message_photo_id_4, message_photo_id_5, message_photo_2, message_photo_3, message_photo_4, message_photo_5, cost,DATE_FORMAT(`date`, '%d.%m.%Y %H:%i:%s') AS `date` FROM vc_users "
                . "WHERE id_users= id_users ".$work
                . " ORDER BY id DESC "
                . " LIMIT ".self::SHOW_BY_DEFAULT
                . " OFFSET ".$offset;
		}
        $clientsList = array();
        
        $result = $db->query($sql);
        $i= 0;
        while ($row = $result->fetch()){
           $clientsList[$i] = $row;
            $i++;
        }
        
        return $clientsList;    
    }
    public static function countClients($id,$work){
        if ($work !== false) {$work = " AND work='".$work."'";} 
        $db = Db::getConnection();

        $result = $db->query("SELECT count(id) AS count FROM vc_users "
                . "WHERE id_users='".$id."'".$work);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];
    }

    public static function checkName($username){
        if (strlen($username)>=3){
            return true;
        }
        return false;
    }
    public static function checkEmail($email){
        if (filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;
    }
    public static function checkPassword($password){
        if (strlen($password)>=6){
            return true;
        }
        return false;
    }
    
    public static function addNewPass($oldpass,$pass,$username){
       
        $db = Db::getConnection();
        $sql="UPDATE `usertbl` SET `password`=".$pass." WHERE password='".$oldpass."' AND `username`='".$username."'";
        
        $result = $db->exec($sql);
        if ($result){
            return true;
        }
        return false;
    }
    public static function editModeration(){  
        $db = Db::getConnection();
        $id=htmlspecialchars($_POST["id"]);

        $result = $db->prepare('SELECT cost, id_users FROM vc_users WHERE id= ?');
        $result->execute([$id]);
        $price = $result->fetch();
 
        $result = $db->prepare('SELECT balance, username FROM usertbl WHERE id= ?');
        $result->execute([$price['id_users']]);
        $balname = $result->fetch();
          
        $bal=$balname['balance']-$price['cost'];
            if($bal>=0){
                $result = $db->prepare('UPDATE vc_users SET work=1 WHERE id= ? AND id_users= ?');
                $result->execute([$id,$_SESSION['userId']]);
                
                header("location:/user/");
            }else{

                echo '<script type="text/javascript">
                alert("Извините но на аккаунте '.$balname["username"].'\nНедостаточно средств для рассылки\nПополните баланс!!!")
                </script>';
                header("refresh: 0; url=/user/");
            }
    }
    public static function editSave(){   
        $db = Db::getConnection();
        //var_dump($_POST);
        // die;
        $title=$db->quote($_POST['title']);
        $text=$db->quote($_POST['text']);
        $photoid=$db->quote($_POST['photoid1']);
        $photosrc=$db->quote($_POST['photosrc1']);
        $photoid2=$db->quote($_POST['photoid2']);
        $photosrc2=$db->quote($_POST['photosrc2']);
        $photoid3=$db->quote($_POST['photoid3']);
        $photosrc3=$db->quote($_POST['photosrc3']);
        $photoid4=$db->quote($_POST['photoid4']);
        $photosrc4=$db->quote($_POST['photosrc4']);
        $photoid5=$db->quote($_POST['photoid5']);
        $photosrc5=$db->quote($_POST['photosrc5']);
        $idlist=$db->quote($_POST['idlist']);
        $id=$db->quote($_POST['id']);
        $ref=$db->quote($_POST['ref']);
        $datatime = $db->quote($_POST['date'].' '.$_POST['hour'].':'.$_POST['min'].':00');
        $r= [];
        preg_match_all('/[0-9]+/',$idlist,$r);
        $cost=count(array_unique($r[0]));
        switch($cost){
            case ($cost <= 500):
            $price = "0.0003";
            break;

            case ($cost <= 750):
            $price = "0.00025";
            break;

            case ($cost >= 751):
            $price = "0.0002";
            break;

        }
        $cost=$cost*$price;
        
        $sql="UPDATE vc_users SET  date=".$datatime.", title=".$title.", text=".$text.", message_photo=".$photosrc.", message_photo_id=".$photoid.", message_photo_2=".$photosrc2.", message_photo_id_2=".$photoid2.", message_photo_3=".$photosrc3.", message_photo_id_3=".$photoid3.", message_photo_3=".$photosrc3.", message_photo_id_3=".$photoid3.", message_photo_4=".$photosrc4.", message_photo_id_4=".$photoid4.", message_photo_5=".$photosrc5.", message_photo_id_5=".$photoid5.", id_send=".$idlist.", cost=".$cost." WHERE id=".$id;
        $result = $db->exec($sql); 
        if ($result){
            return true;
        }
        return false;
    }
    
    
     public static function pause_editSave(){   
        $db = Db::getConnection();
        
        $title=$db->quote($_POST['title']);
        $text=$db->quote($_POST['text']);

        $photoid=$db->quote($_POST['photoid1']);
        $photosrc=$db->quote($_POST['photosrc1']);
        $photoid2=$db->quote($_POST['photoid2']);
        $photosrc2=$db->quote($_POST['photosrc2']);
        $photoid3=$db->quote($_POST['photoid3']);
        $photosrc3=$db->quote($_POST['photosrc3']);
        $photoid4=$db->quote($_POST['photoid4']);
        $photosrc4=$db->quote($_POST['photosrc4']);
        $photoid5=$db->quote($_POST['photoid5']);
        $photosrc5=$db->quote($_POST['photosrc5']);


        $id=$db->quote($_POST['id']);
        $ref=$db->quote($_POST['ref']);
        $datatime = $db->quote($_POST['date'].' '.$_POST['hour'].':'.$_POST['min'].':00');

        

        $sql="UPDATE vc_users SET  date=".$datatime.", title=".$title.", text=".$text.", message_photo=".$photosrc.", message_photo_id=".$photoid.", message_photo_2=".$photosrc2.", message_photo_id_2=".$photoid2.", message_photo_3=".$photosrc3.", message_photo_id_3=".$photoid3.", message_photo_3=".$photosrc3.", message_photo_id_3=".$photoid3.", message_photo_4=".$photosrc4.", message_photo_id_4=".$photoid4.", message_photo_5=".$photosrc5.", message_photo_id_5=".$photoid5." WHERE id=".$id;

        $result = $db->exec($sql); 
        if ($result){
            return true;
        }
        return false;
    }
    
    public static function addNewClient($response,$temp){
        $ran=$temp['ran'];
        $site=$temp['site'];
        $id_users=$temp['id'];
        
        if ($site == 'victoriyaclub') {
            $id = MyFunc::Parse($response, '-ID-', '-');
            $name= MyFunc::Parse($response, 'le>','</');
            $photo= MyFunc::Parse($response,'photos/'.$id.'/','" />');
        } else if ($site == 'loveinchat') {
            $id = MyFunc::Parse($response, '-ID-', '-');
            $name= MyFunc::Parse($response, 'le>','</');
            $photo= MyFunc::Parse($response,'photos/'.$id.'/','" alt');
        } else if ($site == 'tenderbride') {
            $id = MyFunc::Parse($response, 'ACCOUNT №', '</span>');
            $name= MyFunc::Parse($response, 'le>','</');
            $photo= MyFunc::Parse($response,'photos/'.$id.'/','" alt');
        }
        
        
        
        $photo='https://'.$site.'.com/files/users/photos/'.$id.'/'.$photo;

        
        $sql="INSERT INTO vc_users (photo, id_name, name, cookie, site, id_users) VALUES ('$photo','$id', '$name', '$ran','$site','$id_users')";

        echo $sql;
        
        $db = Db::getConnection();
  
        $result = $db->exec($sql);
        if ($result){
            return true;
        }
        return false;
        
        return $sql;
    }
    
    
    public static function accountInfo(){
       
        $db = Db::getConnection();
        
        $sql = 'SELECT * FROM usertbl WHERE `id`= :id';
        
        $result = $db->prepare($sql);
        $result->bindParam(':id', $_SESSION['userId'], PDO::PARAM_STR);
        $result->execute();
        
        $accountInfo = $result->fetch();
        if ($accountInfo){
            return $accountInfo;
        }
        return false;
    }
    
    public static function pause($id){
       
        $db = Db::getConnection();
        
        $sql = "UPDATE vc_users SET work=6 WHERE id = ?";
        
        $result = $db->prepare($sql);
        $result->execute([$id]);
    }
    
    public static function start($id){
       
        $db = Db::getConnection();
        
        $sql = "UPDATE vc_users SET work=5 WHERE id = ?";
        
        $result = $db->prepare($sql);
        $result->execute([$id]);
    }

    public static function checkNameExits($username){
        
        $db = Db::getConnection();
        
        $sql = 'SELECT COUNT(*) FROM usertbl WHERE `username`= :username';
        
        $result = $db->prepare($sql);
        $result->bindParam(':username', $username, PDO::PARAM_STR);
        $result->execute();
        
        if ($result->fetchColumn()){
            return true;
        }
        return false;
    }
    

    public static function checkEmailExits($email){
        
        $db = Db::getConnection();
        
        $sql = 'SELECT COUNT(*) FROM usertbl WHERE `email`= :email';
        
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        
        if ($result->fetchColumn()){
            return true;
        }
        return false;
    }
    
    public static function checkUserData($username,$password){
        
        $db = Db::getConnection();
        
        $sql = 'SELECT id FROM usertbl WHERE `username`= :username AND `password`= :password';
        
        $result = $db->prepare($sql);
        $result->bindParam(':username', $username, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->execute();
        
        $userId = $result->fetch();
        if ($userId){
            return $userId['id'];
        }
        return false;
    }
    
    public static function auth($userId){
        session_start();
        $_SESSION['userId']=$userId;
    }
    public static function logout(){
        session_start();
        unset($_SESSION['userId']);
        session_destroy();
        header("location:/user/login/");
    }

    public static function checkLogger(){
        session_start();
        
        if (isset($_SESSION['userId'])){
            return $_SESSION['userId'];
        }
        header("Location: /user/login/");
    }
    
    public static function register($username,$email,$password){
        $db = Db::getConnection();
        
        $sql = 'INSERT INTO usertbl (username, email, password, type)' .
                'VALUES (:username, :email, :password, 1)';
        
        $result = $db->prepare($sql);
        $result->execute([$username,$email,$password]);
        
        $to = $email;
        $subject = 'Регистрация на SMC.pp.ua';
        $message = "Вы получили это письмо, так как этот e-mail адрес был использован при регистрации на сайте. \r\n
------------------------------------------------
Ваш логин и пароль на сайте:
------------------------------------------------
Логин: ".$username."
Пароль: ".$password."
Благодарим Вас за регистрацию.
С уважением,
Администрация https://smc.pp.ua .";
        $headers = 'From: support@smc.pp.ua' . "\r\n";

        mail($to, $subject, $message, $headers);
    }
}