<?php


class MyFunc {

    public static function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {

        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }

        foreach ( $arrays AS $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
            if ( is_array( $value ) OR is_object( $value )  ) {
                MyFunc::http_build_query_for_curl( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
    }
    
    public static function siteSend($url,$type='GET',$ran,$data=[]){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url ); // отправляем на
        curl_setopt($ch, CURLOPT_HEADER, 0); // пустые заголовки
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвратить то что вернул сервер
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // следовать за редиректами
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);// таймаут4
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// просто отключаем проверку сертификата
//curl_setopt($ch, CURLOPT_INTERFACE, "176.114.14.238");
        if ($type=='POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            //$post = array();
            //MyFunc::http_build_query_for_curl($data, $post);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'].'/c/o/o/k/i/e/'.$ran.'cookie.php'); // сохранять куки в файл
        curl_setopt($ch, CURLOPT_COOKIEFILE,  $_SERVER['DOCUMENT_ROOT'].'/c/o/o/k/i/e/'.$ran.'cookie.php');

        $res=curl_exec($ch);
        curl_close($ch);
        
        
        return $res;
    }
    
    public static function Parse($p1, $p2, $p3) {
	$pos = strpos($p1, $p2)+strlen($p2);
	if ($pos === false) return 0;
	$p1 = substr($p1, $pos);
	$pos = strpos($p1, $p3);
	$p1 = substr($p1, 0, $pos);
	return htmlspecialchars($p1);
    }
}
