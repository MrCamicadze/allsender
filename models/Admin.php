<?php


class Admin {
    
    public static function checkLogAdmin(){
        $db = Db::getConnection();
        
        $sql = 'SELECT * FROM usertbl WHERE `id`= :id';
        
        $result = $db->prepare($sql);
       // $result->bindParam(':id', $_SESSION['userId'], PDO::PARAM_STR);
        $result->execute([$_SESSION['userId']]);
        
        $accountInfo = $result->fetch();
        if ($accountInfo['type']==3){
            return $accountInfo;
        }

        header("Location: /user/");
    }
    public static function getUser($type){
        $db = Db::getConnection();
        
        if ($_SESSION['userId'] !== '62') {
            if ($type == 'all'){
            $sql = "SELECT id, username, type, balance FROM usertbl WHERE username <> 'root' ORDER BY balance DESC";
                $stmt = $db->query($sql);
            }else{
                $sql = "SELECT id, username, type, balance FROM usertbl WHERE id = ?";
                $stmt = $db->prepare($sql);
                $stmt->execute([$type]);
            }
        } else {
            if ($type == 'all'){
            $sql = "SELECT id, username, type, balance FROM usertbl ORDER BY balance DESC";
                $stmt = $db->query($sql);
            }else{
                $sql = "SELECT id, username, type, balance FROM usertbl WHERE id = ?";
                $stmt = $db->prepare($sql);
                $stmt->execute([$type]);
            }
        }
        return $stmt->fetchall();
    }
    
    public static function deleteUser($id){
        $db = Db::getConnection();
            
        // $id=htmlspecialchars($_POST['id']);
        $result = $db->prepare('DELETE FROM usertbl WHERE id=?');
        $result->execute([$id]);
    }
    
    public static function getPayAll(){
        $db = Db::getConnection();
        
        $sql = "SELECT `balance_control`.`id`,DATE_FORMAT(`datetime`, '%d.%m.%Y %H:%i:%s') AS `datetime`, "
                . "`usertbl`.`username`, `balance_control`.`value`, "
                . "`balance_control`.`stat`  "
                . "FROM `balance_control` "
                . "LEFT JOIN `usertbl` "
                . "ON `balance_control`.`id_users` = `usertbl`.`id` "
                . "WHERE `balance_control`.`stat`!='' "
                . "ORDER by `balance_control`.`id` desc LIMIT 5 ";
        $result = $db->query($sql);
        
        return $result->fetchAll();
       
    }
    public static function getPayDays($day1,$day2){
        $db = Db::getConnection();
       
        
        $sql = "SELECT DATE_FORMAT(date, '%d.%m.%Y') AS date, round(SUM(cost),2) AS cost "
                . "FROM vc_users "
                . "WHERE date>=:day1 and date<=:day2";
      
      
        $result = $db->prepare($sql);
        $result->execute([$day1,$day2]);
        $getPay = $result->fetch();
        if ($getPay['date'] == date("d.m.Y", strtotime($day1))){
            return $getPay;
        }else{
            return ['date'=>date("d.m.Y", strtotime($day1)),'cost'=>'0'];
        }
    }
    
    public static function saveInfoUser($username,$type,$balance,$id){
        $db = Db::getConnection();
        
        $sql = "UPDATE `usertbl` SET `username`=:username,`type`=:type,`balance`=:balance WHERE `id`=:id";
        $result = $db->prepare($sql);
        $result->execute([$username,$type,$balance,$id]);
    }
    
    public static function infoClientsAll(){      
        $db = Db::getConnection();

        
        $sql = "SELECT `vc_users`.id,site,username,"
                . " DATE_FORMAT(`date`, '%H:%i:%s %d.%m.%Y') AS date,"
                . " id_name, work, work_from, "
                . "work_to, cost "
                . "FROM vc_users "
                . "LEFT JOIN `usertbl` "
                . "ON `vc_users`.`id_users` = `usertbl`.`id` "
                . "WHERE work='5' or work='3'  "
                . "ORDER BY date DESC ";
        
      
        
        
        $clientsList = array();
        
        $result = $db->query($sql);
        $i= 0;
        while ($row = $result->fetch()){
           $clientsList[$i] = $row;
            $i++;
        }
        
        return $clientsList;    
    }
    
    
    
}
