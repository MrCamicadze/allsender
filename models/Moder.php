<?php


class Moder {

    public static function checkLogModer(){
        $db = Db::getConnection();
        
        $sql = 'SELECT * FROM usertbl WHERE `id`= :id';
        
        $result = $db->prepare($sql);
       // $result->bindParam(':id', $_SESSION['userId'], PDO::PARAM_STR);
        $result->execute([$_SESSION['userId']]);
        
        $accountInfo = $result->fetch();
        if (($accountInfo['type']==2) or ($accountInfo['type']==3)){
            return $accountInfo;
        }

        header("Location: /user/");
    }
    public static function moderationClients($id){     
        $db = Db::getConnection();
        
        $sql = "SELECT id,site, photo,id_name, name, title, text, "
                . "message_photo, message_photo_2, message_photo_3, message_photo_4,  message_photo_5, cost FROM vc_users WHERE work=1 and id_users = $id ORDER BY id DESC";
        $clientsList = array();
        
        $result = $db->query($sql);
        $i= 0;
        while ($row = $result->fetch()){
           $clientsList[$i] = $row;
            $i++;
        }
        
        return $clientsList;
    }
    
    public static function moderationAdminClients(){     
        $db = Db::getConnection();
        
        $sql = "SELECT id,site, photo,id_name, name, title, text, "
                . "message_photo, message_photo_2, message_photo_3, message_photo_4,  message_photo_5, cost FROM vc_users WHERE work=1 ORDER BY id DESC";
        $clientsList = array();
        
        $result = $db->query($sql);
        $i= 0;
        while ($row = $result->fetch()){
           $clientsList[$i] = $row;
            $i++;
        }
        
        return $clientsList;
    }
    public static function editModerOk(){  
                
        $db = Db::getConnection();
        $id=htmlspecialchars($_POST["id"]);

        
        $result = $db->prepare('SELECT id_send,title,text, cost, id_users, date FROM vc_users WHERE id=?');
        $result->execute([$id]);
        $data = $result->fetch();

        if (strlen($data['title'])==0){
            echo '<script type="text/javascript">
                alert("Поле: Тема сообщения - пустое!")
                </script>';
            header("refresh: 0; url=/moder/");
            return;
        }
        if (strlen($data['text'])==0){
            echo '<script type="text/javascript">
                alert("Поле: Сообщение - пустое!")
                </script>';
            header("refresh: 0; url=/moder/");
            return;
        }
        
        $result1 = $db->prepare('SELECT balance, username FROM usertbl WHERE id= ?');
        $result1->execute([$data['id_users']]);
        $balname = $result1->fetch();
          
        $bal=$balname['balance']-$data['cost'];
            if($bal>=0){
                $result = $db->prepare('UPDATE usertbl SET balance= ? WHERE id= ?');
                $result->execute([$bal,$data['id_users']]);
                
                $response=[];
                preg_match_all('/[0-9]+/',$data["id_send"],$response);
                $r=array_unique($response[0]);

                foreach ($r as $k=>$e) {
                            if ($k!='0'){
                                $sql=$sql.",('".$e."' , '".$id."')";
                            }else{
                                $sql="INSERT INTO work (id_send,id_users) VALUES ('".$e."' , '".$id."')";
                            } 
                }
                $c=count($r);
                $db->query($sql);
                //var_dump($sql);
                if (is_null($data['date'])){
                    $dat=date('Y-m-d H:i:s');
                    $result2 = $db->prepare('UPDATE vc_users SET work=3, work_to= ? , date= ? WHERE id= ?');
                    $result2->execute([$c,$dat,$id]);
                } else {
                    $result2 = $db->prepare('UPDATE vc_users SET work=3, work_to= ? WHERE id= ?');
                    $result2->execute([$c,$id]);
                }
                header("location:/moder/");     
                
                return true;
            }else{
                echo '<script type="text/javascript">
                alert("Извините но на аккаунте '.$balname["username"].'\nНедостаточно средств для рассылки\nПополните баланс!!!")
                </script>';
                header("refresh: 0; url=/moder/");
            }
         
    }
    
    public static function editModerDelete(){
            $db = Db::getConnection();
            $id=htmlspecialchars($_POST['id']);
            $result = $db->prepare('DELETE FROM vc_users WHERE id=?');
            $result->execute([$id]);
            header("location:/moder/");
    
    }
    public static function addStatisicUsers(){
            $db = Db::getConnection();
            $id=htmlspecialchars($_POST['id']);
            $date = date('Y-m-d');
            $result = $db->prepare('INSERT INTO statistic_post '
                                . '(id_post, date) VALUES ( ? , ?)');
            $result->execute([$id,$date]);
            header("location:/moder/");  
    
    }
}
