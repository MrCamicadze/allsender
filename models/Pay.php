<?php


class Pay {
    
    const public_key = 'i57101866616';
    const private_key = '2HTlrzEPCOdi5ddNnzMGwmDw5T5tGLvCJ6HrEPwj';
    
    
    
    public static function addPayClients($user_id,$amount){     
        $db = Db::getConnection();
        
        $sql = 'INSERT INTO balance_control (id_users, `value`)' .
                'VALUES (:user_id, :amount)';
        
        $result = $db->prepare($sql);
        $result->execute([$user_id,$amount]);     
        return $db->lastInsertId();
    }
    
    public static function checkPayClients($signature,$data){
        
        $sign = base64_encode( sha1( 
                    Pay::private_key .  
                    $data . 
                    Pay::private_key 
                    , 1 ));
        if ($signature==$sign) {
            return true;
        } else {
            return false;
        }
    }
    
    public static function editPayInfo($status,$order){
        $db = Db::getConnection();

        $sql = 'UPDATE balance_control'
                . ' SET stat=:status'
                . ' WHERE id=:order';
        
        $result = $db->prepare($sql);
        $result->execute([$status,$order]);     
    }
    
    public static function editUserBalance($order){
        $db = Db::getConnection();

        $ssql = 'SELECT id_users,value '
                . 'FROM `balance_control` '
                . 'WHERE id=:order';
        
        $result = $db->prepare($ssql);
        $result->execute([$order]);
        $date = $result->fetch();
        
        $usql = 'UPDATE usertbl '
                . 'SET balance=balance+:value '
                . 'WHERE id=:id_user';
        $uresult = $db->prepare($usql);
        $uresult->execute([$date['value'],$date['id_users']]);
    }

    public static function payInfo($user_id){
        $db = Db::getConnection();
        
        $sql ="SELECT * FROM `balance_control` "
                . "WHERE stat!='' "
                . "AND id_users =:user_id "
                . "ORDER BY `datetime` DESC"; 
    
        $result = $db->prepare($sql);
        $result->execute([$user_id]);
        
        $payInfo = $result->fetchAll();
        if ($payInfo){
            return $payInfo;
        }
        return false;
        
   
    }

    public static function clientsForm($amount){
            $order_id = Pay::addPayClients($_SESSION['userId'],$amount);
            $liqpay = new LiqPay(Pay::public_key, Pay::private_key);
            
            return $liqpay->cnb_formJS([
                'action'         => 'pay',
                'amount'         => $amount,
                'currency'       => 'USD',
                'description'    => 'Пополнение аккаунта',
                'order_id'       => $order_id,
                'version'        => '3',
               // 'sandbox'        => '1',
                'server_url'     => 'https://smc.pp.ua/pay/check/'
                ]);       
    }

}
