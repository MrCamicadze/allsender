<?php


class Stat {
    public static function getStatClients($id){     
        $db = Db::getConnection();
        if (isset($_POST['month']) and isset($_POST['year'])){
            $predate = $_POST['year'].'-'.$_POST['month'].'-01';
            $posdate = $_POST['year'].'-'.$_POST['month'].'-31';
        } else {
            
            $predate = date('Y-m').'-01';
            $posdate = date('Y-m').'-31';
        }
        $result = $db->prepare('SELECT id,id_name,site,photo,name,work_from,date,cost FROM vc_users WHERE work=9 AND id_users= ? AND date>= ? AND date<= ? ORDER by cost DESC');
        $result->execute([$id,$predate,$posdate]);
       
        $clientsList = array();
        while ($row = $result->fetch()){
           if (!isset($clientsList[$row['id_name']])){
           $clientsList[$row['id_name']] = ['id'=>$row['id_name'],'site'=>$row['site'],'photo'=>$row['photo'],'name'=>$row['name']];
           }
           $clientsList[$row['id_name']][stat][]= ['id'=>$row['id'],'work_from'=>$row['work_from'],'cost'=>$row['cost'],'date'=>$row['date']];
        
      //  $clientsList[$row['id_name']]=array('id'=>$row['id_name'],'name'=>$row['name'],'test'=>array(array('id'=>$row['id'],'work_from'=>$row['work_from'],'cost'=>$row['cost'])));
            
            
        }
        return $clientsList;
    }
    public static function getStatCost($id){     
        $db = Db::getConnection();
        if (isset($_POST['month']) and isset($_POST['year'])){
            $predate = $_POST['year'].'-'.$_POST['month'].'-01';
            $posdate = $_POST['year'].'-'.$_POST['month'].'-31';
        } else {
            
            $predate = date('Y-m').'-01';
            $posdate = date('Y-m').'-31';
        }
        $result = $db->prepare('SELECT cost FROM vc_users WHERE id_users= ? AND date>= ? AND date<= ? ORDER by cost DESC');
        $result->execute([$id,$predate,$posdate]);
       
        
        $clientsCost = 0;
        while ($row = $result->fetch()){
           $clientsCost += $row['cost'];
        }
        $clientsCost = ($clientsCost > 0) ? $clientsCost : 0;
        return $clientsCost;
    }
}
