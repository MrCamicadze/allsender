<?php


/**
 * Description of PayController
 *
 * @author MaLuTkA
 */
class PayController {
    
    public function actionIndex(){

        User::checkLogger();
        
        $accountInfo= User::accountInfo();

        $amount = filter_input(INPUT_POST, 'amount',FILTER_VALIDATE_FLOAT);
    
        if($amount){          
            $html = Pay::clientsForm($amount);
            require_once(ROOT . '/views/pay/pay.php');
        } else {
            $payInfo = Pay::payInfo($accountInfo['id']);
            return  require_once(ROOT . '/views/pay/index.php');
        }

    }
    
    public function actionCheck(){
        $signature = filter_input(INPUT_POST, 'signature');
        $data = filter_input(INPUT_POST, 'data');
        if (Pay::checkPayClients($signature,$data)){
            $datadecode = json_decode(base64_decode($data), true);
            $status = $datadecode['status']; 
            $order = $datadecode['order_id']; 
            Pay::editPayInfo($status, $order);
            if (($status == 'success') or ($status == 'success')){
                Pay::editUserBalance($order);
            }
            
        }
          
    }
}
