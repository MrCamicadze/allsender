<?php


class AdminController {
    
    public function actionIndex(){
 User::checkLogger();
        $accountInfo= Admin::checkLogAdmin();
        $save       = filter_input(INPUT_POST, 'save', FILTER_VALIDATE_INT);
        $username   = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $type       = filter_input(INPUT_POST, 'type', FILTER_VALIDATE_INT);
        $balance    = filter_input(INPUT_POST, 'balance', FILTER_VALIDATE_FLOAT);
        if (($save) and($username) and ($type) and isset($balance)) {
            Admin::saveInfoUser($username, $type, $balance, $save);
        }
        
             
        

        $usersinfo = Admin::getUser('all');
        $paysInfo = Admin::getPayAll();
        $payDays = [];
        for ($i = 1; $i <= date('d'); $i++) {
            if ($i<10){
                $i='0'.$i;
            }
        $payDays[$i]= Admin::getPayDays('2020-08-'.$i.' 00:00:00','2020-08-'.$i.' 23:59:59');
        }



         $clientsList= Admin::infoClientsAll();


        require_once(ROOT . '/views/admin/index.php');
    }
    
    public function actionEdit(){
        User::checkLogger();
        $accountInfo= Admin::checkLogAdmin(); 
        $edit = filter_input(INPUT_POST, 'edit', FILTER_SANITIZE_STRING);
        $delete = filter_input(INPUT_POST, 'delete', FILTER_SANITIZE_STRING);
        $id = filter_input(INPUT_POST, 'id' ,FILTER_VALIDATE_INT);
        if (($id)and ($edit=='ok')){
            $userinfo = Admin::getUser($id);
            require_once(ROOT . '/views/admin/edit.php');
        }
        
        if (($id) and ($delete=='ok')){
            $userinfo = Admin::deleteUser($id);
            require_once(ROOT . '/views/admin/');
        }

    }
}
