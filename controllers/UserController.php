<?php


class UserController {
    public function actionIndex(){
        
        User::checkLogger();
        
        $accountInfo= User::accountInfo();
        
        $actionId=isset($_GET['id']) ? $_GET['id'] : '1';
        $actionPage=isset($_GET['page']) ? $_GET['page'] : '1';

        $clientsList= User::infoClientsAll($accountInfo['id'], User::checkActionId($actionId),$actionPage);
        $total=User::countClients($accountInfo['id'], User::checkActionId($actionId));
        $pagination = new Pagination($total, $actionPage, User::SHOW_BY_DEFAULT, 'page-');
        require_once(ROOT . '/views/user/index.php');
    }
    
    public function actionEdit(){ 
        User::checkLogger();
        $accountInfo= User::accountInfo();
        if (filter_input(INPUT_POST,'v') == 'edit') {
            $id = htmlspecialchars($_POST['id']);
            $editClient = User::getOneClient($id);
            if (filter_input(INPUT_POST,'myedit')=='Save'){
                if (User::editSave()){
                    header("location:".filter_input(INPUT_POST,'ref'));
                }
            }
            require_once(ROOT . '/views/user/edit.php');
        }
        if (filter_input(INPUT_POST,'v')=='go'){
            User::editModeration();
        }
        if (filter_input(INPUT_POST,'v')=='start'){
            $id = filter_input(INPUT_POST, 'id',FILTER_VALIDATE_INT);
            User::start($id);
            header("location: /user/");
        }
        if (filter_input(INPUT_POST,'v')=='pause'){
            $id = filter_input(INPUT_POST, 'id',FILTER_VALIDATE_INT);
            User::pause($id);
            header("location: /user/");
        }
        if (filter_input(INPUT_POST,'v') == 'pause_edit') {
            $id = htmlspecialchars($_POST['id']);
            $editClient = User::getOneClient($id);
            if (filter_input(INPUT_POST,'myedit')=='Save'){
                if (User::pause_editSave()){
                    header("location:".filter_input(INPUT_POST,'ref'));
                }
            }
            require_once(ROOT . '/views/user/pause_edit.php');
        }
        
        if (filter_input(INPUT_POST,'photo') == 'open'){
            $ran=htmlspecialchars($_POST['ran']);
            $site=htmlspecialchars($_POST['site']);
            $url="https://".$site.".com/js/html/profile/?action=get_photos_to_attach";
            $response = MyFunc::siteSend($url,'GET',$ran);
            $response = preg_replace('/(<in.*">)[\x00-\x1F\x7F]/','',$response);
            $response = preg_replace('/(<bu.*n>)[\x00-\x1F\x7F]/','',$response);
            $response = preg_replace('/\<.*"ckecked_label"\>.*iv\>[\x00-\x1F\x7F]/','',$response);
            // $homepage = file_get_contents('photos.html');
            // echo  $homepage;
            
            echo preg_replace('/src=".*files\//','src="https://'.$site.'.com/files/',$response);
        }
    }
    public function actionAccount(){
        User::checkLogger();
        $accountInfo= User::accountInfo();

        if(isset($_POST['save'])){
            $oldpassword=htmlspecialchars($_POST['oldpassword']);
            $password=htmlspecialchars($_POST['password']);
            $password2=htmlspecialchars($_POST['password2']);

            $errors=false;

            if (!User::checkPassword($oldpassword)){
                $errors[]='Старый пароль не должен быть короче 6-ти символов';
            }
            if (!User::checkPassword($password)){
                $errors[]='Новый пароль не должен быть короче 6-ти символов';
            }
            
            if (($password!=$password2) or (isset($password))){
                $errors[]='Новыe пароли не совпадают!';
            }elseif (!User::addNewPass($oldpassword,$password,$accountInfo['username'])){
                $errors[]='Старый пароль некорректный!';
            }
         

            
        }
        
        
        require_once(ROOT . '/views/user/account.php');
    }
    public function actionCreate(){
        $as=User::checkLogger();
        $accountInfo= User::accountInfo();
        
        
       
        
        
        
        
        if(isset($_POST["mylogin"])){
            $username=htmlspecialchars($_POST['username']);
            $password=htmlspecialchars($_POST['password']);
            $temp['site']=htmlspecialchars($_POST['site']);
            if(!empty($username) && !empty($password)) {
                $temp['id']=$accountInfo['id'];
                $temp['ran']=rand(10000, 99999);
                
                $url = "https://".$temp['site'].".com/js/html/profile/?action=login";
                $data = array(
                        'email'=>$username,
                        'pass'=>$password,
                        
                        'fast'=>1,
                        ); //'g-recaptcha-response'=>'',
                $response = MyFunc::siteSend($url,'POST',$temp['ran'],$data);
   //             var_dump($response);
//var_dump($temp['ran']); 
 // exit();              
                $pos = strpos($response, 'document.location=document.location;');
                if ($pos !== false){
                    $url = 'https://'.$temp['site'].'.com/';
                    $response = MyFunc::siteSend($url,'GET',$temp['ran']);
       //           var_dump('112211');  
     //var_dump($response);
                    $result= User::addNewClient($response,$temp);
                    
                    
                    if($result){
                       header("location: /user/");
                    } else {
                        $message = "Ошибка доступа к базе данных";
                    }
                }else{
                   $message = "Неверное имя пользователя или пароль!"; 
                }
            }else{
                $message = "Все поля обязательны для заполнения!";
            }
       }
        
        
        
        require_once(ROOT . '/views/user/create.php');
		//echo 'Времено не работает';
    }

    public function actionLogin(){
        
        $username='';
        $password='';
                
        if(isset($_POST['login'])){
            $username=htmlspecialchars($_POST['username']);
            $password=htmlspecialchars($_POST['password']);

            $errors=false;
            
            if(!User::checkName($username)){
                $errors[]='Имя не должно быть короче 3-х символов';
            }
            if (!User::checkPassword($password)){
                $errors[]='Пароль не должен быть короче 6-ти символов';
            }
            
            $userId = User::checkUserData($username,$password);
            
            if ($userId == false){
                $errors[] = 'Неправильные данные для входа на сайт';
            }else{
                User::auth($userId);
                
                header("Location: /user/");
            }
            
            
        }
        require_once(ROOT . '/views/user/login.php');
    }
    public function actionRegister(){
        
        $email='';
        $username='';
        $password='';
        $result = false;
        
        if(isset($_POST["register"])){
            $email= filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
            $username=filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $password=filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

            $errors=false;
            if(!User::checkName($username)){
                $errors[]='Имя не должно быть короче 3-х символов';
            }
            if (!User::checkEmail($email)){
                $errors[]='Неправильный Email';
            }
            if (!User::checkPassword($password)){
                $errors[]='Пароль не должен быть короче 6-ти символов';
            }
            if (User::checkNameExits($username)){
                $errors[]='Такой логин уже существует';
            }
            if (User::checkEmailExits($email)){
                $errors[]='Такой email уже существует';
            }
            if ($errors==false){
                User::register($username,$email,$password);
                
                header('Location: /user/login/');
            }
            
        }
        
        require_once(ROOT . '/views/user/register.php');
    }
    
    public function actionLogout(){
        User::logout();
    }
}
