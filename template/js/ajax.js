$('.popup .close_window, .overlay').click(function (){
	$('.popup, .overlay').css({'opacity':'0', 'visibility':'hidden'});
    RemoveClickPhotoEvent();
    AddPreviewPhotoEvent();

});


	

			
$('a.open_window').click(function (e){
    $.ajax({
        type: "POST",
        url: "/user/edit/",
        data: "photo=open&ran="+event.target.id,
                
        success: function(html) {
            
            
            $("#media-library").append(html);
            $("#media-library").find('a').remove();
        }
    });
    RemoveClickPhotoEvent();
    SetClickPhotoEvent();
	$('.popup, .overlay').css({'opacity':'1', 'visibility':'visible'});
	e.preventDefault();
});

$('a.edit_user').click(function (e){
    $.ajax({
        type: "POST",
        url: "/admin/edit/",
        data: "id="+event.target.id,
                
        success: function(html) {
            $("#media-library").empty();
            $("#media-library").append(html);
        }
    });
    
	$('.popup, .overlay').css({'opacity':'1', 'visibility':'visible'});
	e.preventDefault();
});

$("a.delete_user").click(function (e) {
    const shouldDelete = window.confirm(
        "Действительно удалить?"
    );
    

    if (!shouldDelete) {
        return; 
    }
    $.ajax({
        type: "POST",
        url: "/admin/edit/",
        data: "id=" + event.target.id,

        success: function (html) {
        },
    });

    e.preventDefault();
    location.reload(true); 

});

function updateDelPhoto(){

        $('.delphoto').click(function(e){
            e.preventDefault();
            e.stopPropagation();
            var id = $(e.target).attr('id');
            $(`#photoid${id}`).val('');
            $(`#photosrc${id}`).val('');
            $(`#photo${id}`).remove();

        });
  
}

function RemoveClickPhotoEvent(){
    $("body").off('click','.user-photo');
}


function AddPreviewPhotoEvent(){
    $("body").on('click','.user-photo', function(){
        let img = $(this).find('img')[0];
        let src = $(img).attr('src');
        let srcarr = src.split('.');
        let ext = srcarr[srcarr.length-1]
        let bigsrc = src.replace('.'+ext,'_big.'+ext);
        //console.log(bigsrc);
        $("#media-library").empty();
        $("#media-library").append(`<img class="fullimg" src=${bigsrc} />`);
        $('.popup, .overlay').css({'opacity':'1', 'visibility':'visible'});
    });
}


function SetClickPhotoEvent(){
    $("body").on('click','.user-photo',function () {
        if ($(this).attr('data-type') == "video"){
        var photoid = "video_"+$(this).attr('data-id'); 
        } else {
        var photoid = $(this).attr('data-id'); 
        }
        //$("#photoid").val(photoid);
        //$("#photosrc").val($(this).find('img').attr('src'));
    
        var name = $(this).get(0);
        
        //$("#result").empty();
        var ml = $('#result');
        var numItems = ml.find(".user-photo").length;
        var imgs = [];
        ml.find('img').each(function(i){
            imgs.push($(this).attr('src'));
        })
        if(imgs.includes($(this).find('img').attr('src'))){
            alert('Error. This photo is already added!');
            return false;
        }
        if(numItems===5){
            alert('Error. You can add NO more tan 5 photos!');
        } else {
            $("#result").append(name);
            // let id = name.attr('data-id');
            // console.log(id);
            for (let index = 1; index < 6; index++) {
                if($(`#photoid${index}`).val().length===0) {
                    $(`#photoid${index}`).val(photoid);
                    $(`#photosrc${index}`).val($(this).find('img').attr('src'));
                    var targ = $(name).find('.user-photo-img-div')[0];
                    $(targ).prepend('<a href="" class="delphoto" id="'+index+'" >Удалить фото</a>')
                    $(name).attr("id", `photo${index}`);
                    updateDelPhoto();
                    break;
                }
                
            }
            RemoveClickPhotoEvent();
            AddPreviewPhotoEvent();            

        }
    
       $('.popup, .overlay').css({'opacity':'0', 'visibility':'hidden'}); 
    });
}

$( document ).ready(function() {
    updateDelPhoto();
    AddPreviewPhotoEvent();
});
